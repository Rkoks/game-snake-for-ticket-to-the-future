package app;

import java.awt.*;
import java.util.ArrayList;

import additional.Point;

public class Snake {
    public static final Color HEAD_COLOR = Color.blue;

    public static final int LEFT = 37;
    public static final int UP = 38;
    public static final int RIGHT = 39;
    public static final int DOWN = 40;

    private final ArrayList<Point> snake = new ArrayList<>();
    private int direction;

    public Snake(int x, int y, int length, int direction) {
        for (int i = 0; i < length; i++) {
            Point point = new Point(x - i, y);
            snake.add(point);

        }
        snake.get(0).color = HEAD_COLOR;
        this.direction = direction;
    }

    public void move() {
        int x = snake.get(0).getX();
        int y = snake.get(0).getY();

        if (x > GameSnake.FIELD_WIDTH - 1) {
            x = 0;
        }
        if (x < 0) {
            x = GameSnake.FIELD_WIDTH - 1;
        }
        if (y > GameSnake.FIELD_HEIGHT - 1) {
            y = 0;
        }
        if (y < 0) {
            y = GameSnake.FIELD_HEIGHT - 1;
        }

    }

    public void paint(Graphics g) {
        for (Point point : snake) {
            point.paint(g);
        }
    }

    public boolean isInside(int x, int y) {
        for (Point point : snake) {
            if (point.isPoint(x, y)) {
                return true;
            }
        }
        return false;
    }
}
