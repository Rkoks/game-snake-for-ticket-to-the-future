package app;

import additional.Food;
import additional.Point;
import additional.Poison;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Random;

public class GameSnake {
    public static final String TITLE_OF_PROGRAM = "Classic Game app.Snake";
    public static final String GAME_OVER_MSG = "GAME OVER";

    public static final int FIELD_WIDTH = 30;
    public static final int FIELD_HEIGHT = 20;

    public static final int FIELD_DX = 14;
    public static final int FIELD_DY = 37;

    public static final int SHOW_DELAY = 150;

    public static final int START_LOCATION = 200;
    public static final int START_SNAKE_SIZE = 6;
    public static final int START_SNAKE_X = 10;
    public static final int START_SNAKE_Y = 10;

    public static final Color BACKGROUND_COLOR = Color.black;

    public static Food food;
    public static Poison poison;

    public static Snake snake;
    private static JFrame frame;
    private Canvas canvasPanel;
    public static Random random = new Random();

    public static boolean IS_GAME_OVER;
    public static volatile boolean START_NEW_GAME;

    public static void main(String[] args) {

        new GameSnake();

    }

    public GameSnake() {
        startNewGame();

        init();

        go();
    }

    public static void updateTitle(int size) {
        frame.setTitle(TITLE_OF_PROGRAM + " : " + size);
    }

    private void startNewGame() {
        snake = new Snake(START_SNAKE_X, START_SNAKE_Y, START_SNAKE_SIZE, 39);
        IS_GAME_OVER = false;
        START_NEW_GAME = false;
    }

    private void init() {
        frame = new JFrame();
        updateTitle(START_SNAKE_SIZE);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(FIELD_WIDTH * Point.POINT_RADIUS + FIELD_DX, FIELD_HEIGHT * Point.POINT_RADIUS + FIELD_DY);

        frame.setLocation(START_LOCATION, START_LOCATION);
        frame.setResizable(false);

        canvasPanel = new Canvas();
        canvasPanel.setBackground(BACKGROUND_COLOR);

        frame.getContentPane().add(BorderLayout.CENTER, canvasPanel);

        frame.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (IS_GAME_OVER && e.getKeyCode() == 10) {
                    START_NEW_GAME = true;
                }
            }
        });
        frame.setVisible(true);
    }

    private void go() {
        while (true) {
            if (START_NEW_GAME) {
                startNewGame();
                updateTitle(START_SNAKE_SIZE);
            }
            if (!IS_GAME_OVER) {
                snake.move();
                canvasPanel.repaint();
                try {
                    Thread.sleep(SHOW_DELAY);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static class Canvas extends JPanel {
        @Override
        public void paint(Graphics g) {
            super.paint(g);
            snake.paint(g);
            if (IS_GAME_OVER) {
                g.setColor(Color.red);
                g.setFont(new Font("Arial", Font.BOLD, 40));
                FontMetrics fm = g.getFontMetrics();
                g.drawString(GAME_OVER_MSG,
                        (FIELD_WIDTH * Point.POINT_RADIUS + FIELD_DX - fm.stringWidth(GAME_OVER_MSG)) / 2,
                        (FIELD_HEIGHT * Point.POINT_RADIUS + FIELD_DY) / 2);
            }
        }
    }


}
