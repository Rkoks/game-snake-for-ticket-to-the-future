package additional;

import app.GameSnake;

import java.awt.*;

public class Food extends Point {
	public static final Color FOOD_COLOR       = Color.green;

	public Food() {
		super(-1, -1, FOOD_COLOR);
	}

	public void eat() {
		this.setXY(-1, -1);
	}

	public boolean isFood(int x, int y) {
		return isPoint(x, y);
	}

	public boolean isEaten() {
		return this.getX() == -1;
	}

	public void next() {
		int x, y;

		do {
			x = GameSnake.random.nextInt(GameSnake.FIELD_WIDTH);
			y = GameSnake.random.nextInt(GameSnake.FIELD_HEIGHT);
		} while (GameSnake.snake.isInside(x, y));

		this.setXY(x, y);
	}
}
