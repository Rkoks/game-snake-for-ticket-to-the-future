package additional;

import app.GameSnake;

import java.awt.*;
import java.util.ArrayList;

public class Poison {

    private final ArrayList<Point> poisons = new ArrayList<>();
    public static final Color POISON_COLOR = Color.red;

    public boolean isPoison(int x, int y) {
        for (Point poison : poisons) {
            if (poison.isPoint(x, y)) {
                return true;
            }
        }
        return false;
    }

    public void addPoison() {

        int x, y;

        do {
            x = GameSnake.random.nextInt(GameSnake.FIELD_WIDTH);
            y = GameSnake.random.nextInt(GameSnake.FIELD_HEIGHT);
        } while (GameSnake.snake.isInside(x, y) || isPoison(x, y));

        poisons.add(0, new Point(x, y, POISON_COLOR));
    }

    public void paint(Graphics g) {
        for (Point point : poisons) {
            point.paint(g);
        }
    }

}
