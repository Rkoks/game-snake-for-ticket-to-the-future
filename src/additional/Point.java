package additional;

import app.GameSnake;

import java.awt.*;

public class Point {
    public static final int POINT_RADIUS = 20;
    public static final Color DEFAULT_COLOR = Color.yellow;
    private int x, y;
    public Color color = DEFAULT_COLOR;

    public Point(int x, int y) {
        setXY(x, y);
    }

    public Point(int x, int y, Color color) {
        setXY(x, y);
        this.color = color;
    }

    public void paint(Graphics g) {
        g.setColor(color);
        g.fillOval(x * POINT_RADIUS, y * POINT_RADIUS, POINT_RADIUS,
                POINT_RADIUS);
    }

    public boolean isPoint(int x, int y) {
        return this.x == x && this.y == y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setXY(int x, int y) {
        this.x = x;
        this.y = y;
    }
}
